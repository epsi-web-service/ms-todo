package com.todolist.epsi.api_java.todo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoService {
	
	@Autowired
	TodoRepository todoRepository;
	
	public Iterable<Todo> getTodosByUserId(String userId){
		return todoRepository.findByUserId(userId);
	}
	
	public Todo addTodo(String description, String userId) {
		Todo todo = new Todo();
		todo.setDescription(description);
		todo.setDone(false);
		todo.setUserId(userId);
		todoRepository.save(todo);
		return todo;
	}
	
	public Todo toggleDone(long id) {
		Optional<Todo> optTodo = todoRepository.findById(id);
		optTodo.ifPresent(c -> c.toggleDone());
		Todo todo = optTodo.get();
		todoRepository.save(todo);
		return todo;		
	}
	
	public Todo changeDescription(long id, String description) {
		Optional<Todo> optTodo = todoRepository.findById(id);
		optTodo.ifPresent(c -> c.setDescription(description));
		Todo todo = optTodo.get();
		todoRepository.save(todo);
		return todo;
	}

	public void deleteTodo(long id) {
		todoRepository.deleteById(id);
	}

}
