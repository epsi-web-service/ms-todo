package com.todolist.epsi.api_java.todo;

import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Todo, Long>{

	
	Iterable<Todo> findByUserId(String userId);


}
