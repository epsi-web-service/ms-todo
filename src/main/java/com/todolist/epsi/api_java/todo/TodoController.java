package com.todolist.epsi.api_java.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TodoController {
	
	@Autowired
	TodoService service;

	@RequestMapping(value = "/todo", method = RequestMethod.GET)
	public Iterable<Todo> getTodosByUser(@RequestHeader(value="X-Auth-UserId") String userId) {
		return service.getTodosByUserId(userId);
	}
	
	@RequestMapping(value = "/todo", method = RequestMethod.POST)
	public Todo addTodo(@RequestBody String description, @RequestHeader(value="X-Auth-UserId") String userId) {
		return service.addTodo(description, userId);
	}
	
	@RequestMapping(value = "/todo/done/{id}", method = RequestMethod.PATCH)
	@ResponseBody
	public Todo toggleDone(@PathVariable("id") long id) {
		return service.toggleDone(id);
	}
	
	@RequestMapping(value = "/todo/description/{id}", method = RequestMethod.PATCH)
	public Todo changeDescription(@PathVariable("id") long id, @RequestBody String description) {
		return service.changeDescription(id, description);
	}
	
	@RequestMapping(value = "/todo/{id}", method = RequestMethod.DELETE)
	public void addTodo(@PathVariable("id") long id) {
		service.deleteTodo(id);
	}
	
}
