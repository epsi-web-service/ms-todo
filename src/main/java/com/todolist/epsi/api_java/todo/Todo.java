package com.todolist.epsi.api_java.todo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Todo {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;

	@Column(name = "description")
	private String description;

	@Column(name = "done")
	private Boolean done;
	
	@Column(name = "userId")
	private String userId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getDone() {
		return done;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void toggleDone() {
		if(this.done) {
			this.done = false;
		}else {
			this.done = true;
		}
	}
}
