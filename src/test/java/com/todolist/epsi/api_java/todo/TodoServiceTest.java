package com.todolist.epsi.api_java.todo;

import java.util.ArrayList;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TodoServiceTest {
	
	@InjectMocks
	TodoService todoService;
	
	@Mock
	TodoRepository todoRepository;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getTodoByUserTest() {
		// variable
		String userId = "userId";
		Todo todo = new Todo();
		ArrayList<Todo> todos = new ArrayList<Todo>();
		todo.setDescription("description");
		todo.setDone(true);
		todo.setId(1);
		todo.setUserId("userId");
		todos.add(todo);
		
		// mock		
		Mockito.when(todoRepository.findByUserId(userId)).thenReturn(todos);
		
		// method
		Iterable<Todo> result = todoService.getTodosByUserId(userId);
		
		// check
		Assertions.assertThat(result).contains(todo);
	}
	
	@Test
	public void addTodoTest() {
		// variable
		Todo todo = new Todo();
		
		// mock
		Mockito.when(todoRepository.save(todo)).thenReturn(todo);// équivault à ne rien faire
		
		// method
		Todo result = todoService.addTodo("description", "userId");
		
		// check
		Assertions.assertThat(result).hasFieldOrPropertyWithValue("description", "description");
		Assertions.assertThat(result).hasFieldOrPropertyWithValue("done", false);
		Assertions.assertThat(result).hasFieldOrPropertyWithValue("userId", "userId");
	}
	
	@Test
	public void toggleDoneTest() {
		// variable
		Todo todoFalse = new Todo();
		todoFalse.setDone(false);
		Optional<Todo> optTodoFalse = Optional.of(todoFalse);
		Todo todoTrue = new Todo();
		todoTrue.setDone(true);
		Optional<Todo> optTodoTrue = Optional.of(todoTrue);
		
		// mock
		Mockito.when(todoRepository.findById(Mockito.anyLong())).thenReturn(optTodoFalse).thenReturn(optTodoTrue);
		Mockito.when(todoRepository.save(Mockito.any())).thenReturn(null).thenReturn(null);
		
		
		// method
		Todo resultFalse = todoService.toggleDone(1);
		Todo resultTrue = todoService.toggleDone(1);
		
		// check
		Assertions.assertThat(resultFalse).hasFieldOrPropertyWithValue("done", true);
		Assertions.assertThat(resultTrue).hasFieldOrPropertyWithValue("done", false);
	}
	
	@Test
	public void changeDescriptionTest() {
		// variable
		Todo todo = new Todo();
		todo.setDescription("description");
		Optional<Todo> optTodo = Optional.of(todo);
		
		// mock
		Mockito.when(todoRepository.findById(Mockito.anyLong())).thenReturn(optTodo);
		Mockito.when(todoRepository.save(Mockito.any())).thenReturn(null);
		
		// method
		Todo result = todoService.changeDescription(1, "newDescription");
		
		// check
		Assertions.assertThat(result).hasFieldOrPropertyWithValue("description", "newDescription");
	}	
}
