FROM maven:3.3-jdk-8

COPY .mvn .mvn/
COPY docker docker/
COPY src src/
COPY target target/
COPY mvnw mvnw
COPY mvnw.cmd mvnw.cmd
COPY pom.xml pom.xml